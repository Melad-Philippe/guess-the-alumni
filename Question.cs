﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace AUC.GuessTheAlumni
{
    public class Question
    {
        public string Text { get; set; }
        public string AudioFileName { get; set; }
        public List<string> Answers { get; set; }
        public int RightAnswerIndex { get; set; }
        public static List<Question> LoadQuestions()
        {
            var list = new List<Question>();

            #region Question 1 Queen Rania
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Mona Eltahawy", "Queen Rania", "Mona Eltahawy", "Suad Juffali" },
                RightAnswerIndex = 1,
                AudioFileName = "QueenRania.mp3"
            });

            #endregion
            #region Question 2 YuriKoiKi
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Satsuki Katayama", "Munehiro Anzawa", "Yuriko Koiki", "Seiko Noda" },
                RightAnswerIndex = 2,
                AudioFileName = "YurikoKoiki.mp3"
            });


            #endregion
            #region Question 3 Sahar Nasr
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Rania Al-Mashat", "Sahar Nasr", "Inas Abdel-Dayem", "Mervat Tallawy" },
                RightAnswerIndex = 1,
                AudioFileName = "SaharNasr.mp3"
            });

            #endregion
            #region Question 4 Rania Mashaat
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Rania Al Mashaat", "Sahar Nasr", "Mervat Tallawy", "Inas Abdel-Dayem" },
                RightAnswerIndex = 0,
                AudioFileName = "RaniaelMashaat.mp3"
            });

            #endregion // Question 5
            #region Question 10 Rana ELkaliouby
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Mona Eltahawy", "Rania Al-Mashat", "Rana El Kaliouby", "Aya Abdelraouf" },
                RightAnswerIndex = 2,
                AudioFileName = "RanaelKaliouby.mp3"

            });


            #endregion
            #region Question 5 Omar Samra
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Omar Khalifa", "Omar Samra", "Seif Abou Zaid", "Mohamed Rafea " },
                RightAnswerIndex = 1,
                AudioFileName = "OmarSamra.mp3"
            });


            #endregion
            #region Question 6 Lamis Hadidy
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Lamis ElHadidy", "Mona ElShazly", "Riham El Sahly", "Lobna Abdel Aziz " },
                RightAnswerIndex = 0,
                AudioFileName = "Lamis.m4a"
            });

            #endregion   // Question 7
            #region Question 7 Mona EL SHazly
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Lamis ElHadidy", "Lobna Abd El Aziz", "Mona ElShazly", "Riham El Sahly",
                "" },
                RightAnswerIndex = 2,
                AudioFileName = "monaelshazly.mp3"
            });


            #endregion  // Question 8
            #region Question 8 Aser
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Mohamed Imam", "Asser Yassin", "Amr Waked", " Khaled Abol Naga" },
                RightAnswerIndex = 1,
                AudioFileName = "Aser.m4a"

            });
            #endregion // Question 9
            #region Question 9 Amina Khalil
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Angie Wegdan", "May Elghety", "Yosra El Lozy", "Amina Khalil" },
                RightAnswerIndex = 3,
                AudioFileName = "Amina.m4a"
            });


            #endregion  // Question 10
            #region Question 11 Yosreya Loza
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { " Mervat Tallawy", "Raghda Elebrashy", " Suad Juffali", "Yosreya Loza Sawiris" },
                RightAnswerIndex = 3,
                AudioFileName = "yosryaloza.mp3"
            });

            #endregion
            #region Question 12 Loba abdelaziz
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Yosra El Lozy", "Lobna Abd El Aziz", "Angie Wegdan", "Amina Khalil" },
                RightAnswerIndex = 1,
                AudioFileName = "lobnaAbdelaziz.mp3"

            });

            #endregion // Question 13
            #region Question 13 Ben Weideman
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Ben Weideman", "Nicholas Kristof", "Thomas Friedman", "Laurence Wright" },
                RightAnswerIndex = 0,
                AudioFileName = "benwedeman.mp3"

            });

            #endregion  // Question 14

            #region Question 14 Mahmoud el Essiely
            var q14 = new Question();
            q14.Text = "Who is the Speaker?";
            q14.Answers = new List<string> { "Asser Yassin", "Hisham Abbas", "Mahmoud El Esseily", "Hassan Abou El Rous" };
            q14.RightAnswerIndex = 2;
            q14.AudioFileName = "Mahmoudelesseily.mp3";
            #endregion
            #region Question 23 Farida temraza
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Farida Temraza", "Donia Ashry", "Dina El Missiry", "Mounaz Abdelraouf" },
                RightAnswerIndex = 0,
                AudioFileName = "faridatemraza.mp3"
            });

            #endregion
            #region Question 15 TPS Founders
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Saad, Bassem,Tamer", "Omar Khalifa", "Mohamed Rafea", "Omar Samra" },
                RightAnswerIndex = 1,
                AudioFileName = "TBSFounders.mp3"
            });


            #endregion

            #region Question 16 Raghda ELebrashy
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Raghda Elebrashy", "Suad Juffali", "Queen Rania", "Yousriya Loza-Sawiris" },
                RightAnswerIndex = 0,
                AudioFileName = "raghdaelebrashy.mp3"
            });


            #endregion  // Question 17           
            #region Question 17 Mohammed Imam
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Mohamed Imam", "Ramy Imam", "Omar Samra", "Asser Yassin" },
                RightAnswerIndex = 0,
                AudioFileName = "Mohamedemam.mp3"

            });


            #endregion // Question 18         
            #region Question 18 Hisham Abass
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Hisham Abbas", "Mohamed Imam", "Ramy Imam", "Asser Yassin" },
                RightAnswerIndex = 0,
                AudioFileName = "Abbas.m4a"
            });

            #endregion
            #region Question 19 Nicholas Kristof
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Thomas Friedman", "Laurence Wright", "Nicholas Kristof", "Ben Weideman" },
                RightAnswerIndex = 2,
                AudioFileName = "NicholasKristof.mp3"
            });
            #endregion     // Question 20        
            #region Question 20  Thomas friedman
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Ben Weideman", "Nicholas Kristof", "Laurence Wright", "Thomas Friedman" },
                RightAnswerIndex = 3,
                AudioFileName = "Thomasfriedman.mp3"
            });
            #endregion    // Question 21       
            #region Question 21 Ramy Imam
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Mohamed Imam", "Ramy Imam", "Omar Samra", "Asser Yassin" },
                RightAnswerIndex = 1,
                AudioFileName = "RamyEmam.mp3"
            });

            #endregion  // Question 22        
            #region Question 22 LawrenceWright
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Laurence Wright", "Thomas Friedman", "Nicholas Kristof", "Ben Weideman" },
                RightAnswerIndex = 0,
                AudioFileName = "Lawrencewright.mp3"
            });
            #endregion
            #region question 24 Okhteen
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Aya & Mona Abdel Raouf", "Farida and Yasmine Khamis", "Haidy and Helen Ghali ",
                "Nadine and Sara Aboulmagd" },
                RightAnswerIndex = 0,
                AudioFileName = "okhteinfounders.mp3"
            });


            #endregion // Question 25      
            #region Question 25 HaifaaMansour
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Hajar Alnaim", "Haifaa Almansour", "Hanaa Saleh al-Fassi", "Reem Al-Bayyat" },
                RightAnswerIndex = 1,
                AudioFileName = "haifaaelMansour.mp3"
            }
            );
            #endregion // Question 26     
            #region Question 26 Yasssen AbdelGhaffar
            list.Add(new Question()
            {
                Text = "Who is the Speaker?",
                Answers = new List<string> { "Ahmed Zahran", "Mohamed Rafea", "Yaseen Abdelghaffar", " Omar Khalifa" },
                RightAnswerIndex = 2,
                AudioFileName = "yaseenabdelghaffar.mp3"

            });

            #endregion
            return list;
        }
    }
}
