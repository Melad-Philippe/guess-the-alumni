﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AUC.GuessTheAlumni.Pages
{
    /// <summary>
    /// Interaction logic for ScoreGame.xaml
    /// </summary>
    public partial class ScoreGame : Page
    {
        private DispatcherTimer ScoreWaiter;
        public ScoreGame(int MyScore)
        {
            InitializeComponent();
            ScoreWaiter = new DispatcherTimer();
            ScoreWaiter.Interval = TimeSpan.FromSeconds(10);
            ScoreWaiter.Tick += ScoreWaiter_Tick;
            ScoreWaiter.Start();
            lblScore.Content = MyScore;
        }
        private void ScoreWaiter_Tick(object sender, EventArgs e)
        {
            ScoreWaiter.Stop();
            (Application.Current.MainWindow as MainWindow).MainFrame.Navigate(new HomeGame());
        } 
       
    }
}
