﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AUC.GuessTheAlumni.Pages
{
    /// <summary>
    /// Interaction logic for PlayGrid.xaml
    /// </summary>
    public partial class PlayGame : Page
    {
        private DispatcherTimer questionTimer;
        private TimeSpan timerDuration;
        private List<Question> questions;
        int score;
        private int myq;
        private List<int> myquestions = new List<int>();

        private static MediaPlayer player;

        Button CurrentBtn;
        TextBlock CurrentTxt;
        public PlayGame()
        {
            InitializeComponent();
            questions = Question.LoadQuestions();
            //timerDuration = ParseTimeSpan(lblTimer.Content.ToString());
            questionTimer = new DispatcherTimer();
            questionTimer.Interval = TimeSpan.FromSeconds(1);
            questionTimer.Tick += QuestionTimerOnTick;
            questionTimer.Start();
            player = new MediaPlayer();

            // Reset
            myquestions.Clear();
            score = 0;
            lblTimer.Content = "00:30";
            timerDuration = ParseTimeSpan(lblTimer.Content.ToString());
            PlayNextQuestion();
        }

        private void QuestionTimerOnTick(object sender, EventArgs e)
        {
            timerDuration -= TimeSpan.FromSeconds(1);
            lblTimer.Content = timerDuration.ToString("mm\\:ss");

            if (timerDuration.TotalSeconds <= 5)
            {
                lblTimer.Foreground = new SolidColorBrush(Colors.Red);
            }
            if (timerDuration.TotalSeconds < 1)
            {
                player.Stop();
                questionTimer.Stop();
                (Application.Current.MainWindow as MainWindow).MainFrame.Navigate(new ScoreGame(score));
              //  new MainWindow().MainFrame.Navigate(new ScoreGame());
            }
            
        }
        TimeSpan ParseTimeSpan(string time)
        {
            var timeComponents = time.Split(':');

            var minutes = timeComponents.Any() ? int.Parse(timeComponents[0]) : 0;
            var seconds = timeComponents.Count() > 1 ? int.Parse(timeComponents[1]) : 0;

            return new TimeSpan(0, 0, minutes, seconds);
        }
        private void Choice_Click(object sender, RoutedEventArgs e)
        {

            var q = questions[myq];
            player.Stop();

            CurrentBtn = (e.OriginalSource as Button);
            CurrentTxt = ((e.OriginalSource as Button).Content as TextBlock);
            var answer = ((e.OriginalSource as Button).Content as TextBlock).Text;
            if (q.Answers[q.RightAnswerIndex] == answer)
            {
                (e.OriginalSource as Button).Background = new SolidColorBrush(Colors.Green);
                ((e.OriginalSource as Button).Content as TextBlock).Foreground = new SolidColorBrush(Colors.White);
                score += 100;
            }
            else
            {
                (e.OriginalSource as Button).Background = new SolidColorBrush(Colors.Red);
                ((e.OriginalSource as Button).Content as TextBlock).Foreground = new SolidColorBrush(Colors.White);
            }
            var timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += (o, args) =>
            {
                timer.Stop();
                CurrentBtn.Background = new SolidColorBrush(Colors.White);
                CurrentTxt.Foreground = new SolidColorBrush(Colors.Black);
                PlayNextQuestion();
            };
            timer.Start();
            
        }
        private void PlayNextQuestion()
        {
            DoubleAnimation Opacityanimate = new DoubleAnimation();
            Opacityanimate.From = 1;
            Opacityanimate.To = 0;
            Opacityanimate.Duration = new Duration(TimeSpan.Parse("00:00:1"));
            Opacityanimate.AutoReverse = false;

            QuestionWrapper.BeginAnimation(WrapPanel.OpacityProperty, Opacityanimate);


            if (myquestions.Count < questions.Count)
            {
                Random rnd = new Random();
                myq = rnd.Next(0, questions.Count() - 1);
                while (myquestions.Contains(myq))
                {
                    myq = rnd.Next(0, questions.Count);
                }
                myquestions.Add(myq);

                choice1.Text = questions[myq].Answers[0];
                choice2.Text = questions[myq].Answers[1];
                choice3.Text = questions[myq].Answers[2];
                choice4.Text = questions[myq].Answers[3];

                Opacityanimate.From = 0;
                Opacityanimate.To = 1;
                QuestionWrapper.BeginAnimation(WrapPanel.OpacityProperty, Opacityanimate);

                player.Open(new Uri(string.Concat("Audio/", questions[myq].AudioFileName), UriKind.RelativeOrAbsolute));
                player.Volume = 1;
                player.Play();
            }
            else
            {
                player.Stop();
                (Application.Current.MainWindow as MainWindow).MainFrame.Navigate(new ScoreGame(score));
            }

        }
    }
}
